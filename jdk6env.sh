#PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games"
PATH="/opt/jdk1.6.0_26/bin:/opt/jdk1.6.0_26/jre/bin:/opt/arm-eabi-4.4.3/bin:/opt/android-sdk-linux-x86/tools:/opt/android-sdk-linux-x86/platform-tools:~/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games"

JAVA_HOME="/opt/jdk1.6.0_26"
JRE_HOME="/opt/jdk1.6.0_26/jre"
ARM_EABI_HOME="/opt/arm-eabi-4.4.3"
ANDROID_SDK_HOME="/opt/android-sdk-linux-x86"
ANDROID_JAVA_HOME="/opt/jdk1.6.0_26"

#PATH=$JAVA_HOME/bin:$JRE_HOME/bin:$ARM_EABI_HOME/bin:$ANDROID_SDK_HOME/tools:$ANDROID_SDK_HOME/platform-tools:$PATH
export JAVA_HOME ANDROID_JAVA_HOME JRE_HOME PATH
