#!/bin/bash
# $1 - project name : e.g. e903, e906
# $2 - gprs or wcdma
# $3 - gemini or not : gemini = d, single = s
# $4 - customer name : e.g. lifan
# $5 - customer project name : e.g. a6


if [ "$2" != "w" ] ; then
	now_date_2=`date +v2g.%y.%m.%d`
else
	now_date_2=`date +v3g.%y.%m.%d`
fi


if [ "$5" = "common" ] ; then
    custom_mobile_type=$1
else
    custom_mobile_type=$5
fi

cp customize/$1-$2-$3-$4-$5/ProjectConfig.mk.txt ./.env
sed -i "s/ = /=/g" .env
sed -i "s/ =/=/g" .env
source .env
echo $CUSTOM_MODEM
rm .env

if [ "$MTK_EMMC_SUPPORT" != "yes" ] ; then
	mcp_type=nand
else
	mcp_type=emmc
fi

if [ "$2" != "w" ] ; then
	exit 1
else
	bb_type=mt6589w
fi

out_update_zip_name=${bb_type}-$4-${custom_mobile_type}-$2-$3-$now_date_2-update



if [ $# -ne 5 ] ; then 
	echo "ERROR : You just need give project name and customer mobile type!"
else


	./pre-build.sh $1 $2 $3 $4 $5 && ./mk -o=TARGET_BUILD_VARIANT=user $1 new && ./mk -o=TARGET_BUILD_VARIANT=user $1 otapackage && ./build/tools/releasetools/ota_from_target_files -l out/target/product/$1/logo.bin -u out/target/product/$1/lk.bin out/target/product/$1/obj/PACKAGING/target_files_intermediates/$1-target_files-user.$USER.zip ${out_update_zip_name}.zip && ./mzTarget $1 $2 $3 $4 $5 

#	./build/tools/releasetools/ota_from_target_files -l out/target/product/$1/logo.bin -u out/target/product/$1/lk.bin out/target/product/$1/obj/PACKAGING/target_files_intermediates/$1-target_files-user.$USER.zip ${out_update_zip_name}.zip


fi
