#!/bin/bash
# $1 - project name : e.g. e903, e906
# $2 - gprs or wcdma
# $3 - gemini or not : gemini = d, single = s
# $4 - customer name : e.g. lifan
# $5 - customer project name : e.g. a6

./pre-build.sh $1 $2 $3 $4 $5 && ./mk -o=TARGET_BUILD_VARIANT=user $1 n && ./mzTarget $1 $2 $3 $4 $5
